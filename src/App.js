import React, { Component } from 'react';
import './App.scss';
import SideBar from './components/sidebar/SideBar';
import Header from './components/header/Header';
import Main from './components/main/Main';

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faBell, faCog, faChevronUp, faChevronDown, faPlusCircle, faCheck }
  from '@fortawesome/free-solid-svg-icons'
import RecoverPassword from './components/recoverPassword/RecoverPassword';

library.add(faBell, faCog, faChevronUp, faChevronDown, faPlusCircle, faCheck)

class App extends Component {
  render() {
    return (
      <section className="App">
        <SideBar></SideBar>
        <article className="content">
          <Header></Header>
          <Main></Main>
          <RecoverPassword></RecoverPassword>
        </article>
      </section>
    );
  }
}

export default App;
