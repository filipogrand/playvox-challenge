import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class StatusButton extends Component {
    constructor(props) {
        super(props)

        this.state = {
            activeStatus: false,
            statusList: [
                { id: 'all', value: 'All Status', active: 'active', options: [] },
                { id: 'equal', value: 'Equal To', active: '', options: ['Solved', 'Pending', 'Closed', 'Hold'] },
                { id: 'not_equal', value: 'Not equal to', active: '', options: ['Solved', 'Pending', 'Closed', 'Hold'] },
                { id: 'like', value: 'Like', active: '', options: ['Solve', 'Pend', 'Close', 'Holded'] },
            ],
            selectedStatus: 'All Status',
            activeStatusOption: false,
            selectedStatusOption: 'Select Status',
            filterSelected: '',
            activateValidation: false
        }
    }

    clickStatus = (isDone) => {
        this.setState({ activeStatus: !this.state.activeStatus });
        const optionSeleted = this.state.selectedStatusOption;
        const statusListActive = this.state.statusList.filter(
            status => { return status.active === 'active' })[0]

        

        let labelState = (this.state.selectedStatusOption !== 'Select Status')
            ? `Status ${(statusListActive.value).toLowerCase()}:
                ${this.state.selectedStatusOption}`
            : statusListActive.value

        if (isDone === true) {
            if(statusListActive.id !== 'all') {
                if(this.state.selectedStatusOption === 'Select Status') {
                    this.setState({ activateValidation: true });
                    this.setState({ selectedStatus: 'Status is: missing value' });
                    return;   
                }
            }
            this.setState({ activateValidation: false });

            this.setState({ selectedStatus: labelState });
            const statusSelected = this.state.statusList.filter( (item) => item.active === 'active' )[0].id;
            this.setState({ filterSelected:
                { operator: statusSelected, option: optionSeleted.toLowerCase(), filter:'status' } }, 
                () =>  this.props.onActivateFilter(this.state.filterSelected) );
        }
    }

    selectStatus = (currentStatus) => {
        const statusListValues = ['All Status', 'Equal To', 'Not equal to', 'Like'];
        const newStatusList = this.state.statusList.map((obj, key) =>
            (key === currentStatus)
                ? { id: obj.id, value: statusListValues[key], active: 'active', options: obj.options }
                : { id: obj.id, value: statusListValues[key], active: '', options: obj.options }
        );
        this.setState({ statusList: newStatusList });
        this.setState({ selectedStatusOption: 'Select Status' });
    }

    selectOption = (currentOption, item) => {
        this.setState({ selectedStatusOption: currentOption });
        const newStatusList = this.state.statusList.map((obj, key) =>
            (key === 1 && item.id === 'equal')
                ? { id: obj.id, value: 'Is', active: obj.active, options: obj.options }
                : { id: obj.id, value: obj.value, active: obj.active, options: obj.options }
        );

        this.setState({ statusList: newStatusList })
        this.clickStatusOption();
    }

    clickStatusOption = () => {
        this.setState({ activeStatusOption: !this.state.activeStatusOption })
    }

    render() {
        let activeClass, chevronDirection, activeFilterOption;
        if (this.state.activeStatus) {
            activeClass = 'customFilter customButton active';
            chevronDirection = 'chevron-up';
            activeFilterOption = 'activeFilterOption active';
            activeClass += (this.state.activateValidation) ? ' error' : ''
        } else {
            activeClass = 'customFilter customButton';
            chevronDirection = 'chevron-down';
            activeFilterOption = 'activeFilterOption';
            activeClass += (this.state.activateValidation) ? ' error' : ''
        }

        let activeClassOptions, chevronDirectionOption, activeFilterOptionInside;
        if (this.state.activeStatusOption) {
            activeClassOptions = 'customFilter customButton active';
            chevronDirectionOption = 'chevron-up';
            activeFilterOptionInside = 'activeFilterOptionInside active';

        } else {
            activeClassOptions = 'customFilter customButton';
            chevronDirectionOption = 'chevron-down';
            activeFilterOptionInside = 'activeFilterOptionInside';
        }

        return (
            <div className={(this.state.showStatus === true) ? 'showStatus active' : 'showStatus'}>
                <button className={activeClass} onClick={this.clickStatus}>
                    <span className="customLabel">{this.state.selectedStatus}</span>
                    <span className="chevron">
                        <FontAwesomeIcon icon={['fas', chevronDirection]} />
                    </span>
                </button>
                <div className={activeFilterOption}>
                    <ul className="listFiltersOptions">
                        <li className={this.state.statusList[0].active}
                            onClick={() => this.selectStatus(0)}>
                            {this.state.statusList[0].value}<FontAwesomeIcon icon={['fas', 'check']} />
                        </li>

                        {this.state.statusList.map((item, key) => {
                            if (key > 0) {
                                return (
                                    <li key={key} className={item.active}>
                                        <div className="listItemRow" onClick={() => this.selectStatus(key)}>
                                            <div className="contentStatus">
                                                <span>
                                                    <div className="iconBox"></div>
                                                </span>
                                                {item.value}
                                            </div>
                                            <FontAwesomeIcon icon={['fas', 'check']} />
                                        </div>
                                        <div className="listItemRow options">
                                            <button className={activeClassOptions} onClick={this.clickStatusOption}>
                                                <span className="customLabel">{this.state.selectedStatusOption}</span>
                                                <span className="chevron">
                                                    <FontAwesomeIcon icon={['fas', chevronDirectionOption]} />
                                                </span>
                                            </button>
                                            <div className={activeFilterOptionInside}>
                                                <ul className="listFiltersOptions">
                                                    {item.options.map((option, keyInside) => {
                                                        return (<li key={keyInside} onClick={() => this.selectOption(option, item)}>{option}</li>);
                                                    })}
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                )
                            } else { return '' }
                        })}
                        <li onClick={() => this.clickStatus(true)}>
                            <button>Done</button>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default StatusButton
