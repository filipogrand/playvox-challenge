import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class AddFilterButton extends Component {
    constructor(props) {
        super(props)

        this.state = {
            filters: ['Tags', 'Priority', 'Customer Satisfaction', 'Metrics'],
            activeAddFilter: false,
            activeAddFilterSelect: '',
            showCustomerSatisfaction: false,
            customerSatisfactionList: [
                { id: 'greater_tan', value: 'Greater than', active: 'active' },
                { id: 'lower_than', value: 'Lower than', active: '' },
                { id: 'is', value: 'Is', active: '' },
                { id: 'is_not', value: 'Is not', active: '' }
            ],
            percentage: 90,
            selectedCustomerSatisfaction: 'CSAT greater than: 90%',
            filterSelected: ''
        }
    }

    clickCustomerSatisfaction = (isDone) => {

        this.setState({ activeCustomerSatisfaction: !this.state.activeCustomerSatisfaction });
        let labelCustomerSatisfection =
            `CSAT ${(this.state.customerSatisfactionList.filter(
                customerSatisfaction => { return customerSatisfaction.active === 'active' })[0].value).toLowerCase()}:
                ${this.state.percentage}%`

        if (isDone === true) {
            this.setState({
                selectedCustomerSatisfaction: labelCustomerSatisfection
            });

            const csatSelected = this.state.customerSatisfactionList.filter(
                customerSatisfaction => { return customerSatisfaction.active === 'active' })[0].id;

            this.setState({ filterSelected:
                { operator: csatSelected, option: parseInt(this.state.percentage), filter:'csat' } }, 
                () =>  this.props.onActivateFilter(this.state.filterSelected) );
        }
    }

    selectCustomerSatisfaction = (currentCustomerSatisfaction) => {
        const currentSatisfactionListValues = ['Greater than', 'Less than', 'Is', 'Is not'];
        const newCustomerSatisfactionList = this.state.customerSatisfactionList.map((obj, key) =>
            (key === currentCustomerSatisfaction)
                ? { id: obj.id, value: currentSatisfactionListValues[key], active: 'active' }
                : { id: obj.id, value: currentSatisfactionListValues[key], active: '' }
        );

        this.setState({ customerSatisfactionList: newCustomerSatisfactionList });
        this.setState({ selectedStatusOption: 'Select Status' });
    }

    addFilter = () => {
        this.setState({ activeAddFilter: !this.state.activeAddFilter });
    }

    selectFilter = (filter) => {
        this.addFilter();
        this.setState({ activeAddFilterSelect: filter });
        if (filter === 'Customer Satisfaction') {
            this.setState({ showCustomerSatisfaction: true });
        }
    }

    handlePercentageChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    }

    render() {

        let activeClass, chevronDirection, activeFilterOption;
        if (this.state.activeCustomerSatisfaction) {
            activeClass = 'customFilter customButton active';
            chevronDirection = 'chevron-up';
            activeFilterOption = 'activeFilterOption active';
        } else {
            activeClass = 'customFilter customButton';
            chevronDirection = 'chevron-down';
            activeFilterOption = 'activeFilterOption';
        }

        let activeAddFilter;
        if (this.state.activeAddFilter) {
            activeAddFilter = 'addFilterList active';
        } else {
            activeAddFilter = 'addFilterList';
        }

        return (
            <React.Fragment>
                <div
                    className={
                        (this.state.showCustomerSatisfaction === true) ? 'showCustomerSatisfaction active' : 'showCustomerSatisfaction'}>
                    <button className={activeClass} onClick={this.clickCustomerSatisfaction}>
                        <span className="customLabel">{this.state.selectedCustomerSatisfaction}</span>
                        <span className="chevron">
                            <FontAwesomeIcon icon={['fas', chevronDirection]} />
                        </span>
                    </button>
                    <div className={activeFilterOption}>
                        <ul className="listFiltersOptions">
                            {this.state.customerSatisfactionList.map((item, key) => {
                                return (
                                    <li key={key} className={item.active}>
                                        <div className="listItemRow" onClick={() => this.selectCustomerSatisfaction(key)}>
                                            <div className="contentStatus">
                                                <span>
                                                    <div className="iconBox"></div>
                                                </span>
                                                {item.value}
                                            </div>
                                            <FontAwesomeIcon icon={['fas', 'check']} />
                                        </div>
                                        <div className="listItemRow options">
                                            <input
                                                type="number"
                                                minLength="0"
                                                maxLength="100"
                                                name="percentage"
                                                className="inputPercentage"
                                                value={this.state.percentage}
                                                onChange={this.handlePercentageChange} >
                                            </input>
                                            <span className="percentage">%</span>
                                        </div>
                                    </li>)
                            })}

                            <li onClick={() => this.clickCustomerSatisfaction(true)}>
                                <button>Done</button>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="addFilterContent">
                    <button className="addFilter customButton" onClick={this.addFilter}>
                        <span><FontAwesomeIcon icon={['fas', 'plus-circle']} /></span>
                        <span className="customLabel">Add filter</span>
                    </button>
                    <section className={activeAddFilter}>
                        <ul>
                            {this.state.filters.map((filter, key) => {
                                return (<li key={key} onClick={() => this.selectFilter(filter)}>{filter}</li>)
                            })}
                        </ul>
                    </section>
                </div>
            </React.Fragment>
        )
    }
}

export default AddFilterButton
