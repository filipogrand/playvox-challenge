import React, { Component } from 'react'
import './recoverPassword.scss'
import recoverPasswordRequest from './../../assets/json/recover_password_request.json';
import TableUsers from './TableUsers.js';
import StatusButton from './StatusButton';
import AddFilterButton from './AddFilterButton';

class RecoverPassword extends Component {

    constructor(props) {
        super(props)

        this.state = {
            response: recoverPasswordRequest
        }
    }


    handleActivateFilter = (filterSelected) => {

        switch (filterSelected.operator) {
            /* Cases for Status */
            case 'all': return this.setState({ response: recoverPasswordRequest });
            case 'equal': return this.setState({
                response: this.state.response.filter((item) => { return (item[filterSelected.filter] === filterSelected.option) })
            });
            case 'not_equal': return this.setState({
                response: this.state.response.filter((item) => { return (item[filterSelected.filter] !== filterSelected.option) })
            });
            case 'like': return this.setState({
                response: this.state.response.filter((item) => { return (item[filterSelected.filter].match(filterSelected.option)) })
            });

            /* Cases for CSAT */
            case 'greater_tan': return this.setState({
                response: this.state.response.filter((item) => { return (item[filterSelected.filter] > filterSelected.option) })
            });
            case 'lower_than': return this.setState({
                response: this.state.response.filter((item) => { return (item[filterSelected.filter] < filterSelected.option) })
            });
            case 'is': return this.setState({
                response: this.state.response.filter((item) => { return (item[filterSelected.filter] === filterSelected.option) })
            });
            case 'is_not': return this.setState({
                response: this.state.response.filter((item) => { return (item[filterSelected.filter] !== filterSelected.option) })
            });

            default: return this.setState({ response: recoverPasswordRequest });
        }
    }

    render() {

        return (
            <section id="recoverPassword">
                <h2 className="subtitle">Recover password request</h2>
                <StatusButton onActivateFilter={this.handleActivateFilter}></StatusButton>
                <AddFilterButton onActivateFilter={this.handleActivateFilter}></AddFilterButton>
                <section className="totalRequest">
                    <span>{this.state.response.length} request</span>
                </section>
                <TableUsers
                    request={this.state.response}
                    headers={
                        [
                            { value: 'username', label: 'User Name' },
                            { value: 'subject', label: 'Subject' },
                            { value: 'csat', label: 'Customer Satisfaction' },
                            { value: 'priority', label: 'Priority' },
                            { value: 'status', label: 'Status' }
                        ]
                    }
                ></TableUsers>
            </section >
        )
    }
}

export default RecoverPassword
