import React from 'react'
import { Table } from 'react-bootstrap';

function TableUsers(props) {
    return (
        <section className='tableResults'>
            <Table responsive className='results'>
                <thead>
                    <tr>
                        {props.headers.map((head, key) => {
                            return (<th key={key}>{head.label}</th>)
                        })}
                    </tr>
                </thead>
                <tbody>
                    {props.request.map((item, key) => {
                        return (<tr key={key}>
                            {props.headers.map((head, keyHead) => {
                                return (
                                    <td key={keyHead}> {(() => {
                                        switch (head.value) {
                                            case 'username':
                                                return (<div>
                                                    <span className='iconName'>
                                                        <div className='iconBox'></div>
                                                    </span>{item[head.value]}
                                                </div>);
                                            case 'priority':
                                                return (<div className='priority'>
                                                    <span className={item[props.headers[3].value]}></span>
                                                    {item[head.value]}
                                                </div>);
                                            case 'csat':
                                                return (<div>{item[head.value]}%</div>);
                                            default:
                                                return (<div>{item[head.value]}</div>);
                                        }
                                    })()}
                                    </td>)
                            })}
                        </tr>
                        )
                    })}
                </tbody>
            </Table>
        </section>
    )
}

export default TableUsers
