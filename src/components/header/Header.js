import React, { Component } from 'react'
import './header.scss'

class Header extends Component {
  render() {
    return (
      <header>
          <nav>
              <a href="#Wordload" id="Wordloads" className="mr-34">Wordloads</a>
              <a href="#Interaction" id="Interactions" className="mr-30">Interactions</a>
              <a href="#Evaluation" id="Evaluations" className="mr-34">Evaluations</a>
              <a href="#Report" id="Reports" className="mr-34 active">Reports</a>
              <a href="#Scorecard" id="Scorecards" className="mr-30">Scorecards</a>
              <a href="#Calibration" id="Calibrations" className="mr-34">Calibrations</a>
          </nav>
          <hr/>
      </header>
    )
  }
}

export default Header
