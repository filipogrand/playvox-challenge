import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './sidebar.scss'

class SideBar extends Component {

    render() {
        return (
            <aside id="asideBar">
                <section id="startOption" className="options">
                    <a id="mainLogo" href="/">
                        <img src="./favicon.png" alt="playvox-logo"></img>
                    </a>
                    <nav id="navOptions">
                        <div className="active"></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </nav>
                </section>
                <section id="endOptions" className="options">
                    <button className="bell"><FontAwesomeIcon icon={['fas', 'bell']} /></button>
                    <button className="cog"><FontAwesomeIcon icon={['fas', 'cog']} /></button>
                    <div className="profilePicture"></div>
                </section>
            </aside>
        )
    }
}

export default SideBar
