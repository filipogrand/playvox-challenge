import React, { Component } from 'react'
import './main.scss'

class Main extends Component {
    render() {
        return (
            <article id="mainContent">
                <h1 className="title">Integrating Playvox with Customer Interaction platforms</h1>
                <article className="description">
                    <div>
                        <div className="wrapDescription">
                            <h2 className="subtitle">Everything you need to know</h2>
                            <p>In the process to configuring PlayVox, you will want to setup and integration with order vendor platform to pull in user information and to pull in customer interactions to be evaluated. We have simplied this integration with a few steps.</p>
                        </div>
                    </div>
                    <div>
                        <div className="wrapDescription">
                            <h2 className="subtitle">Filter for custumer interactions</h2>
                            <p>This filters allow you to pull in customer intractions of highest priority for evaluation. The available filters will correspond to those filters or fiels already available for within a 3rd party vendor platform.</p>
                        </div>
                    </div>
                </article>
                <hr/>
            </article>
        )
    }
}

export default Main
