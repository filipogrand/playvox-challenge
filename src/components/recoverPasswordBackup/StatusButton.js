import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class StatusButton extends Component {
    constructor(props) {
        super(props)

        this.state = {
            activeStatus: false,
            statusList: [
                { value: 'All Status', active: 'active', options: [] },
                { value: 'Equal To', active: '', options: ['Solved', 'Pending', 'Closed', 'Hold'] },
                { value: 'Not equal to', active: '', options: ['Solved', 'Pending', 'Closed', 'Hold'] },
                { value: 'Like', active: '', options: ['Solved', 'Pending', 'Closed', 'Hold'] },
            ],
            filters: ['Tags', 'Priority', 'Customer Satisfaction', 'Metrics'],
            selectedStatus: 'All Status',
            activeStatusOption: false,
            selectedStatusOption: 'Select Status'
        }
    }

    clickStatus = (isDone) => {
        this.setState({ activeStatus: !this.state.activeStatus });
        let labelState = (this.state.selectedStatusOption !== 'Select Status')
            ? `Status ${(this.state.statusList.filter(
                status => { return status.active === 'active' })[0].value).toLowerCase()}:
                ${this.state.selectedStatusOption}`
            : this.state.statusList.filter(
                status => { return status.active === 'active' })[0].value

        if (isDone) {
            this.setState({
                selectedStatus: labelState
            })
        }
    }

    selectStatus = (currentStatus) => {
        const statusListValues = ['All Status', 'Equal To', 'Not equal to', 'Like'];
        const newStatusList = this.state.statusList.map((obj, key) =>
            (key === currentStatus)
                ? { value: statusListValues[key], active: 'active', options: obj.options }
                : { value: statusListValues[key], active: '', options: obj.options }
        );

        this.setState({ statusList: newStatusList });
        this.setState({ selectedStatusOption: 'Select Status' });
    }

    selectOption = (currentOption, statusId) => {
        this.setState({ selectedStatusOption: currentOption });

        const newStatusList = this.state.statusList.map((obj, key) =>
            (key === statusId)
                ? { value: 'Is', active: obj.active, options: obj.options }
                : { value: obj.value, active: obj.active, options: obj.options }
        );

        this.setState({ statusList: newStatusList })
        this.clickStatusOption();
    }

    clickStatusOption = () => {
        this.setState({ activeStatusOption: !this.state.activeStatusOption })
    }

    render() {
        let activeClass, chevronDirection, activeFilterOption;
        if (this.state.activeStatus) {
            activeClass = 'customFilter customButton active';
            chevronDirection = 'chevron-up';
            activeFilterOption = 'activeFilterOption active';
        } else {
            activeClass = 'customFilter customButton';
            chevronDirection = 'chevron-down';
            activeFilterOption = 'activeFilterOption';
        }

        let activeClassOptions, chevronDirectionOption, activeFilterOptionInside;
        if (this.state.activeStatusOption) {
            activeClassOptions = 'customFilter customButton active';
            chevronDirectionOption = 'chevron-up';
            activeFilterOptionInside = 'activeFilterOptionInside active';

        } else {
            activeClassOptions = 'customFilter customButton';
            chevronDirectionOption = 'chevron-down';
            activeFilterOptionInside = 'activeFilterOptionInside';
        }

        return (
            <div className={(this.state.showStatus === true) ? 'showStatus active' : 'showStatus'}>
                <button className={activeClass} onClick={this.clickStatus}>
                    <span className="customLabel">{this.state.selectedStatus}</span>
                    <span className="chevron">
                        <FontAwesomeIcon icon={['fas', chevronDirection]} />
                    </span>
                </button>
                <div className={activeFilterOption}>
                    <ul className="listFiltersOptions">
                        <li className={this.state.statusList[0].active}
                            onClick={() => this.selectStatus(0)}>
                            {this.state.statusList[0].value}<FontAwesomeIcon icon={['fas', 'check']} />
                        </li>

                        {this.state.statusList.map((item, key) => {
                            if (key > 0) {
                                return (
                                    <li key={key} className={item.active}>
                                        <div className="listItemRow" onClick={() => this.selectStatus(key)}>
                                            <div className="contentStatus">
                                                <span>
                                                    <div className="iconBox"></div>
                                                </span>
                                                {item.value}
                                            </div>
                                            <FontAwesomeIcon icon={['fas', 'check']} />
                                        </div>
                                        <div className="listItemRow options">
                                            <button className={activeClassOptions} onClick={this.clickStatusOption}>
                                                <span className="customLabel">{this.state.selectedStatusOption}</span>
                                                <span className="chevron">
                                                    <FontAwesomeIcon icon={['fas', chevronDirectionOption]} />
                                                </span>
                                            </button>
                                            <div className={activeFilterOptionInside}>
                                                <ul className="listFiltersOptions">
                                                    {item.options.map((option, key) => {
                                                        return (<li key={key} onClick={() => this.selectOption(option, key)}>{option}</li>);
                                                    })}
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                )
                            } else { return '' }
                        })}
                        <li onClick={() => this.clickStatus(true)}>
                            <button>Done</button>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default StatusButton
