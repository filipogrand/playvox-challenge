import React, { Component } from 'react'
import './recoverPassword.scss'
import recoverPasswordRequest from './../../assets/json/recover_password_request.json';
import TableUsers from './TableUsers.js';
import StatusButton from './StatusButton';
import AddFilterButton from './AddFilterButton';

class RecoverPassword extends Component {

    constructor(props) {
        super(props)

        this.state = {
            activeStatus: false,
            statusList: [
                { value: 'All Status', active: 'active', options: [] },
                { value: 'Equal To', active: '', options: ['Solved', 'Pending', 'Closed', 'Hold'] },
                { value: 'Not equal to', active: '', options: ['Solved', 'Pending', 'Closed', 'Hold'] },
                { value: 'Like', active: '', options: ['Solved', 'Pending', 'Closed', 'Hold'] },
            ],
            filters: ['Tags', 'Priority', 'Customer Satisfaction', 'Metrics'],
            selectedStatus: 'All Status',
            activeStatusOption: false,
            selectedStatusOption: 'Select Status',
            activeAddFilter: false,
            activeAddFilterSelect: '',
            showCustomerSatisfaction: false,
            customerSatisfactionList: [
                { value: 'Greater than', active: 'active' },
                { value: 'Lower than', active: '' },
                { value: 'Is', active: '' },
                { value: 'Is not', active: '' }
            ],
            percentage: 90,
            selectedCustomerSatisfaction: 'CSAT greater than: 90%'
        }
    }

    clickStatus = (isDone) => {
        this.setState({ activeStatus: !this.state.activeStatus });
        let labelState = (this.state.selectedStatusOption !== 'Select Status')
            ? `Status ${(this.state.statusList.filter(
                status => { return status.active === 'active' })[0].value).toLowerCase()}:
                ${this.state.selectedStatusOption}`
            : this.state.statusList.filter(
                status => { return status.active === 'active' })[0].value

        if (isDone) {
            this.setState({
                selectedStatus: labelState
            })
        }
    }

    clickCustomerSatisfaction = (isDone) => {
        this.setState({ activeCustomerSatisfection: !this.state.activeCustomerSatisfection });
        let labelCustomerSatisfection =
            `CSAT ${(this.state.customerSatisfactionList.filter(
                customerSatisfaction => { return customerSatisfaction.active === 'active' })[0].value).toLowerCase()}:
                ${this.state.percentage}%`

        if (isDone) {
            this.setState({
                selectedCustomerSatisfaction: labelCustomerSatisfection
            })
        }
    }

    selectStatus = (currentStatus) => {
        const statusListValues = ['All Status', 'Equal To', 'Not equal to', 'Like'];
        const newStatusList = this.state.statusList.map((obj, key) =>
            (key === currentStatus)
                ? { value: statusListValues[key], active: 'active', options: obj.options }
                : { value: statusListValues[key], active: '', options: obj.options }
        );

        this.setState({ statusList: newStatusList });
        this.setState({ selectedStatusOption: 'Select Status' });
    }

    selectCustomerSatisfaction = (currentCustomerSatisfaction) => {
        const currentSatisfactionListValues = ['Greater than', 'Less than', 'Is', 'Is not'];
        const newCustomerSatisfactionList = this.state.customerSatisfactionList.map((obj, key) =>
            (key === currentCustomerSatisfaction)
                ? { value: currentSatisfactionListValues[key], active: 'active' }
                : { value: currentSatisfactionListValues[key], active: '' }
        );

        this.setState({ customerSatisfactionList: newCustomerSatisfactionList });
        this.setState({ selectedStatusOption: 'Select Status' });
    }

    selectOption = (currentOption, statusId) => {
        this.setState({ selectedStatusOption: currentOption });

        const newStatusList = this.state.statusList.map((obj, key) =>
            (key === statusId)
                ? { value: 'Is', active: obj.active, options: obj.options }
                : { value: obj.value, active: obj.active, options: obj.options }
        );

        this.setState({ statusList: newStatusList })
        this.clickStatusOption();
    }

    clickStatusOption = () => {
        this.setState({ activeStatusOption: !this.state.activeStatusOption })
    }

    addFilter = () => {
        this.setState({ activeAddFilter: !this.state.activeAddFilter });
    }

    selectFilter = (filter) => {
        this.addFilter();
        this.setState({ activeAddFilterSelect: filter });
        if (filter === 'Customer Satisfaction') {
            this.setState({ showCustomerSatisfaction: true });
        }
    }

    handlePercentageChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    }

    render() {

        let activeClass, chevronDirection, activeFilterOption;
        if (this.state.activeStatus) {
            activeClass = 'customFilter customButton active';
            chevronDirection = 'chevron-up';
            activeFilterOption = 'activeFilterOption active';
        } else {
            activeClass = 'customFilter customButton';
            chevronDirection = 'chevron-down';
            activeFilterOption = 'activeFilterOption';
        }

        let activeClass2, chevronDirection2, activeFilterOption2;
        if (this.state.activeCustomerSatisfection) {
            activeClass2 = 'customFilter customButton active';
            chevronDirection2 = 'chevron-up';
            activeFilterOption2 = 'activeFilterOption active';
        } else {
            activeClass2 = 'customFilter customButton';
            chevronDirection2 = 'chevron-down';
            activeFilterOption2 = 'activeFilterOption';
        }

        let activeClassOptions, chevronDirectionOption, activeFilterOptionInside;
        if (this.state.activeStatusOption) {
            activeClassOptions = 'customFilter customButton active';
            chevronDirectionOption = 'chevron-up';
            activeFilterOptionInside = 'activeFilterOptionInside active';

        } else {
            activeClassOptions = 'customFilter customButton';
            chevronDirectionOption = 'chevron-down';
            activeFilterOptionInside = 'activeFilterOptionInside';
        }

        let activeAddFilter;
        if (this.state.activeAddFilter) {
            activeAddFilter = 'addFilterList active';
        } else {
            activeAddFilter = 'addFilterList';
        }

        return (
            <section id="recoverPassword">
                <h2 className="subtitle">Recover password request</h2>
                <StatusButton></StatusButton>
                {/* <div className={(this.state.showStatus === true) ? 'showStatus active' : 'showStatus'}>
                    <button className={activeClass} onClick={this.clickStatus}>
                        <span className="customLabel">{this.state.selectedStatus}</span>
                        <span className="chevron">
                            <FontAwesomeIcon icon={['fas', chevronDirection]} />
                        </span>
                    </button>
                    <div className={activeFilterOption}>
                        <ul className="listFiltersOptions">
                            <li className={this.state.statusList[0].active}
                                onClick={() => this.selectStatus(0)}>
                                {this.state.statusList[0].value}<FontAwesomeIcon icon={['fas', 'check']} />
                            </li>

                            {this.state.statusList.map((item, key) => {
                                if (key > 0) {
                                    return (
                                        <li key={key} className={item.active}>
                                            <div className="listItemRow" onClick={() => this.selectStatus(key)}>
                                                <div className="contentStatus">
                                                    <span>
                                                        <div className="iconBox"></div>
                                                    </span>
                                                    {item.value}
                                                </div>
                                                <FontAwesomeIcon icon={['fas', 'check']} />
                                            </div>
                                            <div className="listItemRow options">
                                                <button className={activeClassOptions} onClick={this.clickStatusOption}>
                                                    <span className="customLabel">{this.state.selectedStatusOption}</span>
                                                    <span className="chevron">
                                                        <FontAwesomeIcon icon={['fas', chevronDirectionOption]} />
                                                    </span>
                                                </button>
                                                <div className={activeFilterOptionInside}>
                                                    <ul className="listFiltersOptions">
                                                        {item.options.map((option, key) => {
                                                            return (<li key={key} onClick={() => this.selectOption(option, key)}>{option}</li>);
                                                        })}
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    )
                                } else { return '' }
                            })}
                            <li onClick={() => this.clickStatus(true)}>
                                <button>Done</button>
                            </li>
                        </ul>
                    </div>
                </div> */}
                <AddFilterButton></AddFilterButton>
                {/* <div
                    className={
                        (this.state.showCustomerSatisfaction === true) ? 'showCustomerSatisfaction active' : 'showCustomerSatisfaction'}>
                    <button className={activeClass} onClick={this.clickCustomerSatisfaction}>
                        <span className="customLabel">{this.state.selectedCustomerSatisfaction}</span>
                        <span className="chevron">
                            <FontAwesomeIcon icon={['fas', chevronDirection]} />
                        </span>
                    </button>
                    <div className={activeFilterOption2}>
                        <ul className="listFiltersOptions">
                            {this.state.customerSatisfactionList.map((item, key) => {
                                return (
                                    <li key={key} className={item.active}>
                                        <div className="listItemRow" onClick={() => this.selectCustomerSatisfaction(key)}>
                                            <div className="contentStatus">
                                                <span>
                                                    <div className="iconBox"></div>
                                                </span>
                                                {item.value}
                                            </div>
                                            <FontAwesomeIcon icon={['fas', 'check']} />
                                        </div>
                                        <div className="listItemRow options">
                                            <input
                                                type="number"
                                                minLength="0"
                                                maxLength="100"
                                                name="percentage"
                                                className="inputPercentage"
                                                value={this.state.percentage}
                                                onChange={this.handlePercentageChange} >
                                            </input>
                                            <span className="percentage">%</span>
                                        </div>
                                    </li>)
                            })}

                            <li onClick={() => this.clickCustomerSatisfaction(true)}>
                                <button>Done</button>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="addFilterContent">
                    <button className="addFilter customButton" onClick={this.addFilter}>
                        <span><FontAwesomeIcon icon={['fas', 'plus-circle']} /></span>
                        <span className="customLabel">Add filter</span>
                    </button>
                    <section className={activeAddFilter}>
                        <ul>
                            {this.state.filters.map((filter, key) => {
                                return (<li key={key} onClick={() => this.selectFilter(filter)}>{filter}</li>)
                            })}
                        </ul>
                    </section>
                </div> */}

                <section className="totalRequest">
                    <span>254 request</span>
                </section>

                <TableUsers
                    request={recoverPasswordRequest}
                    headers={
                        [
                            { value: 'username', label: 'User Name' },
                            { value: 'subject', label: 'Subject' },
                            { value: 'csat', label: 'Customer Satisfaction' },
                            { value: 'priority', label: 'Priority' },
                            { value: 'status', label: 'Status' }
                        ]
                    }
                ></TableUsers>
            </section >
        )
    }
}

export default RecoverPassword
